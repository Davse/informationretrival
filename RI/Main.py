import sys
import math
import operator
from os import listdir
from os.path import isdir
from os.path import isfile
from collections import OrderedDict
from tokenizer.tokenizer import tokenizar
from tokenizer.tokenizer2 import tokenizar2
from StopWords.StopWords import sacar_palabras_vacias

dirname = "../coleccion_2018_final/"
borrarPV = True
archivoPV = "StopWords/StopWords.txt"
lonmax = 40
lonmin = 3
docProcesados = 1
idterm = 0
indice = {}
if (isdir(dirname)):    
        dirs = listdir(dirname)
        terminos = []
        FrecTerm = {}
        print("Indexando Documentos....")
        for file in dirs:
                if isfile(dirname + file):
                    lista_tokens = ""
                    terminosEspLocal={'mails':[], 'urls':[], 'acronimos':[], 'numeros':[], 'nombres':[]}
                    filepath = dirname + file  
                    with open(filepath) as fp:  
                        for line in fp:
                            terminosEspLocal = tokenizar2(line,terminosEspLocal).copy()
                            lista_tokens += tokenizar(line)    
                    fp.close()
                    docProcesados += 1
                    docPalabras = lista_tokens.split()
                    docPalabras = docPalabras + terminosEspLocal['mails'] + terminosEspLocal['urls'] + terminosEspLocal['acronimos'] + terminosEspLocal['numeros'] + terminosEspLocal['nombres']
                    for w in docPalabras:
                        if FrecTerm.get(w):
                            FrecTerm[w] += 1
                        else:
                            FrecTerm[w] = 1
                    Frecaux = FrecTerm.copy()
                    for word in FrecTerm.keys():
                        if len(word)<lonmin or len(word)>lonmax:
                            Frecaux.pop(word)  
                    FrecTerm = Frecaux.copy()         
                    x= open(archivoPV,"r")
                    cadenaPalabras2 = x.read()
                    x.close()
                    vacias = cadenaPalabras2.split()        
                    auxiliar = sacar_palabras_vacias(FrecTerm,vacias).keys()            
                    terminos = list(set(sorted(auxiliar))) 
                for w in terminos:
                    if indice.get(w):
                        indice[w][0] += 1
                        indice[w][1][file] = FrecTerm[w]
                    else:
                        indice[w]= [1,{}]
                        indice[w][1][file] = FrecTerm[w]
print("Canculando Peso de terminos...")                        
for w in indice.keys():
    for f in indice[w][1].keys():
        indice[w][1][f] = indice[w][1][f] * (math.log((docProcesados/indice[w][0]),2))
query = input("Ingrese su busqueda: ")
cleanq = ""
respuesta = {}
terminosEspLocal={'mails':[], 'urls':[], 'acronimos':[], 'numeros':[], 'nombres':[]}
cleanq2 = tokenizar2(query,terminosEspLocal).copy()
cleanq += tokenizar(query)
query = cleanq.split()
query = query + cleanq2['mails'] + cleanq2['urls'] + cleanq2['acronimos'] + cleanq2['numeros'] + cleanq2['nombres']
for q in query:
    if indice.get(q):
        for f in indice[q][1].keys():
            if respuesta.get(f):
                respuesta[f] += indice[q][1][f]
            else:
                respuesta[f] = indice[q][1][f]
ranking = sorted(respuesta.items(), key=operator.itemgetter(1), reverse=True)
for k,v in ranking:
        print(str(k)+"    "+str(v)+"\n")