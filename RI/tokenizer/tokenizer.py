import sys


def tokenizar(text):
    resultado =quitarTildes(caracteresInvalidos(quitarNumeros(pasarMinuscula(text))))
    return resultado
def pasarMinuscula(text):
    return text.lower()

def quitarTildes(text):
    a,b= 'áéíóúüâêîôûàèìòù', 'aeiouuaeiouaeiou'
    trans = str.maketrans(a,b)
    return text.translate(trans)

def caracteresInvalidos(text):
    caracteresInvalidos = '!@#$%^&*()-_=+{}"<>,|\*/~`?¡:;¨´[]¿«».º'
    a,b= caracteresInvalidos + "'", '                                        '
    trans = str.maketrans(a,b)
    return text.translate(trans)

def quitarNumeros(text):
    a,b = '0123456789', '          '
    trans = str.maketrans(a,b)
    return text.translate(trans)