import sys
import re

mail = re.compile(r"[\w]+@{1}[\w]+\.[a-z]{2,3}")
url = re.compile(r"https?:\/\/[\da-z\.-]+\.[a-z\.]{2,6}[\/\w \.-]*\/?")
acronimo1 = re.compile(r"^[A-Z]{1}[a-z]{1,2}\.{1}$")
acronimo2 = re.compile(r"[A-Z]{1}\.{1}[A-Z]{1}\.{1}$")
acronimo3 = re.compile(r"^[A-Z]{2,4}$")
numeros = re.compile(r"^[\+\$]?[\d]+[\.|,|-]?[\d]*$")
nombreG2 = re.compile(r"^[A-Z]{1}[a-z]+[^\f]?[A-Z]{1}[a-z]+$")
nombreG3 = re.compile(r"^[A-Z]{1}[a-z]+[^\f]?[A-Z]{1}[a-z]+[^\f]?[A-Z]{1}[a-z]+$")
def tokenizar2(text,resultado):
    linea = text
    resultado['mails'].extend(get_mail(linea))
    resultado['urls'].extend(get_url(linea))
    resultado['acronimos'].extend(get_abrebiatura(linea))
    resultado['numeros'].extend(get_nro(linea))
    resultado['nombres'].extend(get_nom_propios(linea))
    return resultado
def get_mail(l):
    Lmail = mail.findall(l)
    return Lmail
def get_url(l):
    Lurl = url.findall(l)
    return Lurl
def get_abrebiatura(l):
    return Facronimo1(l) + Facronimo2(l) + Facronimo3(l)	
def Facronimo1(l):
    Lacronimo1 = acronimo1.findall(l)
    return Lacronimo1
def Facronimo2(l):
    Lacronimo2 = acronimo2.findall(l)
    return Lacronimo2
def Facronimo3(l):
    Lacronimo3 = acronimo3.findall(l)
    return Lacronimo3
def get_nro(l):
    Lnumero = numeros.findall(l)
    return Lnumero
def get_nom_propios(l):
    return FnombreG2(l) + FnombreG3(l)
def FnombreG2(l):
    LnombreG2 = nombreG2.findall(l)
    return LnombreG2
def FnombreG3(l):
    LnombreG3 = nombreG3.findall(l)
    return LnombreG3
